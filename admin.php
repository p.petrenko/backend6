<!DOCTYPE html>
<html lang="ru">
<head>
    <title>WEB-Project</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=0.7">
    <link rel="preconnect" href="https://fonts.gstatic.com/">
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@400;500;700&display=swap" rel="stylesheet">
    <link rel="stylesheet"
      href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <script
      src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <link href="style.css" rel="stylesheet">
    <script src="https://code.jquery.com/jquery-1.11.0.min.js"></script>
    <style>
    input[name="birthday"] {
      width: 192px;
    }
    </style>
  </head>
<body>
    <header id="header_site">
      <div id="logo_and_name">
        <img id="logo" src="https://www.flaticon.com/svg/static/icons/svg/203/203678.svg" alt="Logo" />
        <h1>IndexPro</h1>
      </div>
    </header>
    <div class="container py-5 my-3 px-4" id="main">
    <section style="text-align: center">
        <h2>Администрирование</h2>
    </section>
    <br><br>
<?php

if ($_SERVER['REQUEST_METHOD'] == 'GET') {
  if (empty($_SERVER['PHP_AUTH_USER']) ||
      empty($_SERVER['PHP_AUTH_PW'])) {
      header('HTTP/1.1 401 Unanthorized');
      header('WWW-Authenticate: Basic realm="Enter login and password"');
      print('<h1>401 Требуется авторизация</h1>');
      exit();
  }

  $user = 'u20323';
  $pass = '6300522';
  $db = new PDO('mysql:host=localhost;dbname=u20323', $user, $pass, array(PDO::ATTR_PERSISTENT => true));
  
  $trim_login = trim($_SERVER['PHP_AUTH_USER']);
  $trim_password = trim($_SERVER['PHP_AUTH_PW']);
  $stmtCheck = $db->prepare("SELECT * FROM Admin WHERE login = ? AND password_hash = ?");
  $stmtCheck -> execute([$trim_login, substr(hash("sha256", $trim_password), 0, 32)]);
  $db_response = $stmtCheck->fetch();
  
  if (empty($db_response)) {
    header('HTTP/1.1 401 Unanthorized');
    header('WWW-Authenticate: Basic realm="Invalid login or password"');
    print('<h1>401 Неверный логин или пароль</h1>');
    exit();
  }

  $stmtCount = $db->prepare('SELECT ability_name, count(fa.form_id) AS amount FROM Ability AS ab LEFT JOIN Form_Abilities AS fa ON ab.ability_id = fa.ability_id GROUP BY ab.ability_id');
  $stmtCount->execute();
  print('<section>');
  while($row = $stmtCount->fetch(PDO::FETCH_ASSOC)) {
    $current_sp = '';
    if ($row['ability_name'] == 'immortality') 
      $current_sp = 'Бессмертие';
    else if ($row['ability_name'] == 'going_through_walls') 
      $current_sp = 'Прохождение сквозь стены';
    else if ($row['ability_name'] == 'levitation') 
      $current_sp = 'Левитация';
    print('<b>' . $current_sp . '</b>: ' . $row['amount'] . '<br/>');
  }
  print('</section>');

  $stmt1 = $db->prepare('SELECT form_id, name, email, birthday, gender, limb_number, biography, login FROM Form');
  $stmt2 = $db->prepare('SELECT ability_id FROM Form_Abilities WHERE form_id = ?');
  $stmt1->execute();

  while($row = $stmt1->fetch(PDO::FETCH_ASSOC)) {
      print('<br><br>');
      print('<section>');
      print('<h2>' . $row['login'] . '</h2>');
      $superpowers = [false, false, false];
      $stmt2->execute([$row['form_id']]);
      while ($super_row = $stmt2->fetch(PDO::FETCH_ASSOC)) {
          $superpowers[$super_row['ability_id'] - 1] = true;
      }
      include('user_data_form.php');
      print('</section>');
  }
} else {
  if (array_key_exists('delete', $_POST)) {
    $user = 'u20323';
    $pass = '6300522';
    $db = new PDO('mysql:host=localhost;dbname=u20323', $user, $pass, array(PDO::ATTR_PERSISTENT => true));
    $stmt1 = $db->prepare('DELETE FROM Form_Abilities WHERE form_id = ?');
    $stmt1->execute([$_POST['form_id']]);
    $stmt2 = $db->prepare('DELETE FROM Form WHERE form_id = ?');
    $stmt2->execute([$_POST['form_id']]);
    header('Location: admin.php');
    exit();
  }

  $trimmedPost = [];
  foreach ($_POST as $key => $value)
    if (is_string($value))
      $trimmedPost[$key] = trim($value);
    else
      $trimmedPost[$key] = $value;

  if (empty($trimmedPost['name'])) {
    $hasErrors = TRUE;
  }
  $values['name'] = $trimmedPost['name'];

  if (!preg_match('/^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/', $trimmedPost['email'])) {
    $hasErrors = TRUE;
  }
  $values['email'] = $trimmedPost['email'];

  if (!preg_match('/^[0-9]{4}-[0-9]{2}-[0-9]{2}$/', $trimmedPost['birthday'])) {
    $hasErrors = TRUE;
  }
  $values['birthday'] = $trimmedPost['birthday'];

  if (!preg_match('/^[MF]$|^(NB)$/', $trimmedPost['gender'])) {
    $hasErrors = TRUE;
  }
  $values['gender'] = $trimmedPost['gender'];

  if (!preg_match('/^[0-5]$/', $trimmedPost['limb_number'])) {
    $hasErrors = TRUE;
  }
  $values['limb_number'] = $trimmedPost['limb_number'];

  foreach (['1', '2', '3'] as $value) {
    $values['superpowers'][$value] = FALSE;
  }
  if (array_key_exists('superpowers', $trimmedPost)) {
    foreach ($trimmedPost['superpowers'] as $value) {
      if (!preg_match('/[1-3]/', $value)) {
        $hasErrors = TRUE;
      }
      $values['superpowers'][$value] = TRUE;
    }
  }
  $values['biography'] = $trimmedPost['biography'];
  

  if ($hasErrors) {
    // При наличии ошибок перезагружаем страницу и завершаем работу скрипта.
    header('Location: admin.php');
    exit();
  }

  $user = 'u20323';
  $pass = '6300522';
  $db = new PDO('mysql:host=localhost;dbname=u20323', $user, $pass, array(PDO::ATTR_PERSISTENT => true));
  $stmt1 = $db->prepare('UPDATE Form SET name=?, email=?, birthday=?, gender=?, limb_number=?, biography=? WHERE form_id = ?');
  $stmt1->execute([$values['name'], $values['email'], $values['birthday'], $values['gender'], $values['limb_number'], $values['biography'], $_POST['form_id']]);

  $stmt2 = $db->prepare('DELETE FROM Form_Abilities WHERE form_id = ?');
  $stmt2->execute([$_POST['form_id']]);

  $stmt3 = $db->prepare("INSERT INTO Form_Abilities SET form_id = ?, ability_id = ?");
  foreach ($trimmedPost['superpowers'] as $s)
    $stmt3 -> execute([$_POST['form_id'], $s]);

  header('Location: admin.php');
  exit();
}

?>
</div>
    <a id="before_footer"></a>
    <footer id="footer_site">
      <b>@In code we trust</b>
    </footer>
  </body>
</html>
